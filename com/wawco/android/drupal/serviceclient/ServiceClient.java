package com.wawco.android.drupal.serviceclient;
import android.util.Log;

import com.loopj.android.http.*;

public class ServiceClient {
	private final String TAG = "ServiceClient";
	private final String BASE_URL = "http://api.twitter.com/1/";
	private final String CSRF_PATH = "/services/session/token";
	private AsyncHttpClient client = new AsyncHttpClient();
	private String crsfToken = null;
	private static ServiceClient instance = null;
	
	protected ServiceClient() {
		// Singleton; no instantiation
	}
	
	public synchronized static ServiceClient getInstance() {
		return getInstance((PersistentCookieStore) null);
	}
	
	public synchronized static ServiceClient getInstance(PersistentCookieStore cookieStore) {
		// TODO: Refactor instance handling to allow removal of overhead caused
		// by synchronized keyword.  For expediency, we'll go with this for now.
		// Consider switching to a private constructor and using a final static
		//instance variable
		if (instance == null) {
			instance = new ServiceClient();
			if(cookieStore != null) {
				instance.setCookieStore(cookieStore);
			}
		}
		return instance;
	}
	
	protected void addHeaders() {
		// Add the crsfToken to a request
		if (crsfToken == null) {
			// This might need to be refreshed every time?
			getCrsfToken();
		}
		client.addHeader("X-CSRF-Token", crsfToken);
		
	}
	
	private void getCrsfToken() {
		client.get(BASE_URL + CSRF_PATH,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(Throwable error, String responseBody) {
						Log.d(TAG, "CRSF token update request failed. " + responseBody);
						super.onFailure(error, responseBody);
					}

					@Override
					public void onSuccess(String responseBody) {
						Log.d(TAG, "CRSF token update request success: " + responseBody);
						super.onSuccess(responseBody);
					}
			
		});
		
	}

	public void setCookieStore(PersistentCookieStore cookieStore) {
		client.setCookieStore(cookieStore);
	}

	public void makePost(String url, RequestParams params, AsyncHttpResponseHandler responseHandler, PersistentCookieStore cookies) {
		addHeaders();
		client.post(url, params, responseHandler);
	}
	
	public void makeGet(String url, RequestParams params, AsyncHttpResponseHandler responseHandler, PersistentCookieStore cookies) {
		addHeaders();
		client.get(url, params, responseHandler);
	}
	
	public void makePut(String url, RequestParams params, AsyncHttpResponseHandler responseHandler, PersistentCookieStore cookies) {
		addHeaders();
		client.put(url, params, responseHandler);
	}
}
