package com.wawco.android.drupal.userservice;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.wawco.android.drupal.serviceclient.ServiceClient;

public class UserService {
	private static final String USER_PATH = "";
	
	
	public static void userGet(RequestParams user, AsyncHttpResponseHandler handler, PersistentCookieStore cookies) {
		ServiceClient.getInstance().makeGet(USER_PATH, user, handler, cookies);
	}
	public static void userSave(RequestParams user, AsyncHttpResponseHandler handler, PersistentCookieStore cookies) {
		ServiceClient.getInstance().makePost(USER_PATH, user, handler, cookies);
	}
	public static void userRegister(RequestParams user, AsyncHttpResponseHandler handler) {
		
	}
	
	public static void userDelete(RequestParams user, AsyncHttpResponseHandler handler) {
		
	}
	
	public static void userIndex(RequestParams user, AsyncHttpResponseHandler handler) {
		
	}
	
	public static void userLogin(String username, String password, AsyncHttpResponseHandler handler) {
		
	}
}
